﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SimpleShutdown
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static AppStateModel instance = new AppStateModel();

        public MainWindow()
        {
            InitializeComponent();
            LabelCurrentUser.Content = Environment.UserName.ToString();

            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timerTick);
            timer1.Interval = new TimeSpan(0, 0, 1);
            timer1.Start();
        }

        public void timerTick(object sender, EventArgs e)
        {
            LabelCurrentDate.Content = DateTime.Now.ToShortDateString();
            LabelDayOfWeek.Content = DateTime.Now.DayOfWeek.ToString();
            LabelCurrentTime.Content = DateTime.Now.ToLongTimeString();

            if (instance.IsCountdownEnabled)
            {
                LabelIsTimerOff.Content = "Отчет времени идет";
                TimeSpan timeBeforeEnd;
                #region Логика на проверку, если абсолютное время меньше
                if (instance.HourX > DateTime.Now)
                    timeBeforeEnd = instance.HourX - DateTime.Now;
                else
                    timeBeforeEnd = instance.HourX - DateTime.Now.AddDays(-1);
                #endregion
                string parsedTimeSpan = timeBeforeEnd.ToString(@"hh\:mm\:ss");
                LabelTimeBeforeEnd.Content = String.Format("{0:r}", parsedTimeSpan);

                if (instance.HourX.ToLongTimeString() == DateTime.Now.ToLongTimeString())
                {
                    using (var process = new Process())
                    {
                        process.StartInfo.FileName = "cmd.exe";
                        process.StartInfo.Arguments = "/c shutdown /f -s -t 00";
                        process.Start();
                    }
                }
            }
            else
            {
                LabelIsTimerOff.Content = "Таймер остановлен";
                LabelTimeBeforeEnd.Content = "00:00:00";
            }
        }

        private void CheckBoxAbsoluteTime_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxAbsoluteTimeValue.IsEnabled = true;
            instance.IsCountdownEnabled = true;
            CheckBoxTimeTo.IsChecked = false;
            CheckBoxTimeTo.IsEnabled = false;

            try
            {
                instance.HourX = DateTime.Parse(TextBoxAbsoluteTimeValue.Text);
            }
            catch
            {
                ResetTime();
            }
        }

        private void CheckBoxAbsoluteTime_Unchecked(object sender, RoutedEventArgs e)
        {
            TextBoxAbsoluteTimeValue.IsEnabled = false;
            instance.IsCountdownEnabled = false;
            CheckBoxTimeTo.IsEnabled = true;
        }

        /// <summary>
        /// Сброс времени
        /// </summary>
        private void ResetTime()
        {
            MessageBox.Show("Введенное Вами время некорректно, повторите ввод", "Ошибка!");
            instance.HourX = DateTime.Now.AddSeconds(-2);
            TextBoxAbsoluteTimeValue.Text = instance.HourX.ToLongTimeString();
            TextBoxTimeTo.Text = "00:30:00";
        }

        private void CheckBoxTimeTo_Checked(object sender, RoutedEventArgs e)
        {
            instance.IsCountdownEnabled = true;

            TextBoxTimeTo.IsEnabled = true;
            CheckBoxAbsoluteTime.IsChecked = false;
            CheckBoxAbsoluteTime.IsEnabled = false;

            try
            {
                var stringTime = TextBoxTimeTo.Text.ToString();
                TimeSpan additionalTime = TimeSpan.Parse(stringTime);
                instance.HourX = DateTime.Now.Add(additionalTime);
            }
            catch
            {
                ResetTime();
            }
        }

        private void CheckBoxTimeTo_Unchecked(object sender, RoutedEventArgs e)
        {
            instance.IsCountdownEnabled = false;

            TextBoxTimeTo.IsEnabled = false;
            CheckBoxAbsoluteTime.IsEnabled = true;
        }

        private void TextBoxAbsoluteTimeValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                instance.HourX = DateTime.Parse(TextBoxAbsoluteTimeValue.Text);
            }
            catch
            {
                ResetTime();
            }
        }

        private void TextBoxTimeTo_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var stringTime = TextBoxTimeTo.Text.ToString();
                TimeSpan additionalTime = TimeSpan.Parse(stringTime);
                instance.HourX = DateTime.Now.Add(additionalTime).AddSeconds(-2);
            }
            catch
            {
                ResetTime();
            }
        }
    }
}
