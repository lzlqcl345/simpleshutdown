﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShutdown
{
    public class AppStateModel
    {
        /// <summary>
        /// Включен ли таймер на выключение компьютера
        /// </summary>
        public bool IsCountdownEnabled { get; set; }

        /// <summary>
        /// Во сколько нужно будет выключить компьютер 
        /// </summary>
        public DateTime HourX { get; set; }
    }
}
